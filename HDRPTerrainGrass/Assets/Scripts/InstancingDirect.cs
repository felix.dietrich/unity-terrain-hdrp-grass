﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
using System;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Used by TerrainDetailInstancing to instance objects with the GPU.
/// </summary>
public class InstancingDirect : IInstancing
{
    private readonly int BATCH_SIZE = 1023;
    private const float BATCH_SIZE_FLOAT = 1023f;
    private Matrix4x4[] matrices;

    private MaterialPropertyBlock materialProperty;

    private IInstancing.VolatileData dataUpdate;
    private IInstancing.FixedData data;
    private ITerrainDetailComputeKernel computeKernel;

    public InstancingDirect(IInstancing.FixedData newData)
    {
        this.data = newData;
        computeKernel = new TerrainDetailKernelInstancingDirect(this);
    }

    public override IEnumerator UpdateData(IInstancing.VolatileData newDataUpdate)
    {
        materialProperty = new MaterialPropertyBlock();
        materialProperty.SetColor("_BaseColor", data.color);

        this.dataUpdate = newDataUpdate;
        this.dataUpdate.FixData();

        matrices = new Matrix4x4[dataUpdate.positions.Length];

        if(data.windZone == null && !data.isBillboard)
        {
            InitMatrices(dataUpdate.positions, dataUpdate.rotations, dataUpdate.scales);
        }

        yield return null;
    }

    public override void DrawMeshes()
    {
        if (matrices != null)
        {
            int total = matrices.Length;
            int batches = Mathf.CeilToInt(total / BATCH_SIZE_FLOAT);
            var windRotations = new Quaternion[dataUpdate.positions.Length];

            if(data.isBillboard)
            {
                var cameraPos = Camera.main.transform.position;
                for (int idx = 0; idx < dataUpdate.positions.Length; idx++)
                {
                    dataUpdate.rotations[idx] = Quaternion.LookRotation(cameraPos - dataUpdate.positions[idx], Vector3.up);
                }
            }

            if (data.windZone != null)
            {
                UnityEngine.Random.InitState(1);
                for (int idx = 0; idx < dataUpdate.positions.Length; idx++)
                {
                    windRotations[idx] = ApplyWind(data.windZone, dataUpdate.positions[idx], dataUpdate.rotations[idx], data.detailWindStrength);
                }
            }

            // Billboarding and wind zoning is done in code here. InstancingIndirect uses a compute shader for it.
            // In InstancingDirect, We have to update all the matrices every frame.
            if (data.isBillboard || data.windZone != null)
            {
                InitMatrices(dataUpdate.positions, windRotations, dataUpdate.scales);
            }

            for (int i = 0; i < batches; i++)
            {
                int batchCount = Mathf.Min(BATCH_SIZE, total - (BATCH_SIZE * i));
                int start = Mathf.Max(0, i * BATCH_SIZE);

                var batchedMatrices = matrices.Skip(start).Take(batchCount).ToList();
                Graphics.DrawMeshInstanced(data.mesh, 0, data.material, batchedMatrices, materialProperty, data.castShadows);
            }
        }
    }

    public static Quaternion ApplyWind(WindZone zone, Vector3 position, Quaternion rotation, float strength = 1)
    {
        if (zone == null || strength <= 0)
            return rotation;

        var windShift = zone.windPulseMagnitude * Mathf.PerlinNoise(position.x * zone.windTurbulence, position.z * zone.windTurbulence);
        var mainWind = Mathf.Sin(Time.time * zone.windPulseFrequency + 2 * Mathf.PI * windShift);
        var randomWind = Mathf.Cos(Time.time * (UnityEngine.Random.value - .5f) * 2 * Mathf.PI * zone.windTurbulence) * windShift * zone.windTurbulence;
        var waveGrassAmount = zone.windPulseMagnitude * 10;
        var windRotation = Quaternion.AngleAxis(strength * zone.windMain * (mainWind + randomWind) * waveGrassAmount, zone.transform.right);
        return windRotation * rotation;
    }

    private void InitMatrices(IList<Vector3> positions,
                              IList<Quaternion> rotations,
                              IList<Vector3> scales)
    {
        for (int idx = 0; idx < positions.Count; idx++)
        {
            matrices[idx] = Matrix4x4.identity;
            matrices[idx].SetTRS(positions[idx], rotations[idx], scales[idx]);
        }
    }

    public override IEnumerator RecomputeDetailData(int prototypeIndex, Terrain terrain, TerrainDetailInstancing parent)
    {
        yield return computeKernel.RecomputeDetailData(prototypeIndex, terrain, parent);
    }

    public override void Release()
    {
        // from IInstancing.
    }

    public override int Count()
    {
        return this.dataUpdate.positions == null ? 0 : this.dataUpdate.positions.Length;
    }
}
