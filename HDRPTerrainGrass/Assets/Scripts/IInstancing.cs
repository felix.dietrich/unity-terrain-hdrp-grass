using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public abstract class IInstancing
{
    public abstract IEnumerator UpdateData(VolatileData newDataUpdate);
    public abstract void DrawMeshes();
    public abstract void Release();

    public abstract int Count();

    public abstract IEnumerator RecomputeDetailData(int prototypeIndex, Terrain terrain, TerrainDetailInstancing parent);

    public struct FixedData
    {
        public Mesh mesh;
        public Material material;
        public Color color;
        public bool isBillboard;
        public ShadowCastingMode castShadows;
        public WindZone windZone;
        public float detailWindStrength;
    }

    public struct VolatileData
    {
        private List<Vector3> positionList;
        private List<Vector3> scaleList;
        private List<Quaternion> rotationList;

        public Vector3[] positions;
        public Vector3[] scales;
        public Quaternion[] rotations;
        public Bounds bounds;

        public void Clear()
        {
            positionList = new List<Vector3>();
            scaleList = new List<Vector3>();
            rotationList = new List<Quaternion>();
            bounds = new Bounds(Vector3.zero, Vector3.one);
            FixData();
        }

        public void Add(Vector3 position, Vector3 scale, Quaternion rotation)
        {
            positionList.Add(position);
            scaleList.Add(scale);
            rotationList.Add(rotation);
            bounds.Encapsulate(position);
        }

        public void FixData()
        {
            positions = positionList.ToArray();
            scales = scaleList.ToArray();
            rotations = rotationList.ToArray();
        }
    }

    /// <summary>
    /// Size of an element in the compute buffers. Current content:
    /// struct
    /// {
    ///     float4x4 _Matrix;
    ///     float4x4 _InverseMatrix;
    ///     float4 _Info;
    /// }
    /// </summary>
    public static readonly int CB_SIZE = (2 * 4 * 4 + 4) * sizeof(float);

    /// <summary>
    /// Data of a single element in the compute buffers used by the *indirect instancers.
    /// </summary>
    public struct ComputeBufferData
    {
        public Matrix4x4 _Matrix;
        public Matrix4x4 _MatrixInverse;
        public Vector4 _Info;
    }
}
