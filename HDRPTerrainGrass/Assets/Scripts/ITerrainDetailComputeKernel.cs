using System.Collections;
using UnityEngine;

public interface ITerrainDetailComputeKernel
{
    /// <summary>
    /// Recompute the InstanceData for every single DetailMesh in the range of the Camera.main,
    /// for the given prototype (i.e. for a single detail mesh type).
    /// The recomputed InstanceData is sent to the corresponding Instancing object.
    /// </summary>
    /// <param name="prototypeIndex">The index of the prototype detail mesh.</param>
    public IEnumerator RecomputeDetailData(int prototypeIndex, Terrain terrain, TerrainDetailInstancing parent, bool cullByDistance = true);
}
