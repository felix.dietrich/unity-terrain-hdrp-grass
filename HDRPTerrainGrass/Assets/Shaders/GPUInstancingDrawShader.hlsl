#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED

#if defined(SHADER_API_GLCORE) || defined(SHADER_API_D3D11) || defined(SHADER_API_GLES3) || defined(SHADER_API_METAL) || defined(SHADER_API_VULKAN) || defined(SHADER_API_PSSL) || defined(SHADER_API_XBOXONE)
struct CB_Data
{
    float4x4 _Matrix;
    float4x4 _MatrixInverse;
    float4 _Info;
};

uniform StructuredBuffer<CB_Data> _VolatileInstancingData;
#endif	

float4 _PerInstanceInfo;
#endif

void ConfigureProcedural() {

#if defined(UNITY_PROCEDURAL_INSTANCING_ENABLED)
#define unity_ObjectToWorld unity_ObjectToWorld
#define unity_WorldToObject unity_WorldToObject

    unity_ObjectToWorld = _VolatileInstancingData[unity_InstanceID]._Matrix;
    unity_WorldToObject = _VolatileInstancingData[unity_InstanceID]._MatrixInverse;
    _PerInstanceInfo = _VolatileInstancingData[unity_InstanceID]._Info;
#endif
}

void ProceduralInstancing_float(float3 In, out float3 Out, out float4 OutInfo) {
    Out = In;
    OutInfo = 0;
#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
    OutInfo = _PerInstanceInfo;
#endif
}

void ProceduralInstancing_half(half3 In, out half3 Out, out float4 OutInfo) {
    Out = In;
    OutInfo = 0;
#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
    OutInfo = _PerInstanceInfo;
#endif
}