# Unity Terrain HDRP/URP Grass

A demonstration of using GPU instancing for HDRP grass with the old terrain detail instancing system. The code includes a ComputeShader for mesh positioning and additional shader variables per instance.  I tested a URP setup too, it seems to work just as well. The only change necessary is the render target in the Shader Graphs. An example StandardInstancedShader.shader script is also included, to demonstrate how this system works in the standard render pipeline (it does, and it is faster).

You can use everything in the HDRPTerrainGrass/Assets folder, as well as all files in the "Blender models", with the MIT License. Let me know if you do! (https://fd-research.com/contact/) 

Here is a screenshot of the sample scene, with 168FPS on my current system. All objects in the scene are instanced, meaning the Tris/Verts counters are only showing information on the underlying terrain. The given scene shows more than 20,000 detail objects.

* GPU: NVIDIA GTX 1070 (rather old)
* CPU: AMD Ryzen 7 3700X 8-Core (new)
* Unity Version: 2020.3.22f Personal (this is the LTS 2020.3 version); HDRP 10.7.0 lighting.

<img src="wheatbackground.png" alt="wheatbackground" style="zoom:80%;" />

Below is another screenshot with a much larger area: 6000m * 6000m, split up into 9 connected terrains, all with the detail script on them. This scene has roughly 270,000 detail billboard trees, shown at 120FPS (in Editor Play Mode, meaning probably much larger FPS in game).

<img src="terrain_manytrees.png" alt="terrain_manytrees" style="zoom:80%;" />



## Using it in the Unity Editor

You can just use the old terrain detail mesh editor to paint on the terrain. The terrain system will complain that "The current render pipeline does not support Detail shaders", but if you press "Play" or enable`UseInEditor` in the script, the instancing will work anyway. If everything is rendered with the CPU, the standard HDRP/Lit shader suffices (but the FPS is only around ~50 in the example scene).

![terraindetails](terraindetails.png)

Rudimentary wind is also implemented, you have to use a Wind zone and link it to the TerrainDetailInstancing script added to the terrain object.

<img src="terraindetailsinstancing.png" alt="terraindetailsinstancing" style="zoom:100%;" />

These are the public members of the TerrainDetailInstancing.cs script:

```c#
[Tooltip("Controls the detail density on the terrain.")]
[Range(0, 1f)]
public float DetailDensity = 1f;

[Tooltip("Factor to multiply with the terrain detail distance, so that details can be shown much further away.")]
[Range(0, 100f)]
public float DetailDistanceMultiplier = 1f;

[Tooltip("Interval between updates of the Instancing object positions (in seconds).")]
[Range(0, 10f)]
public float UpdateInterval = 5f;

[Tooltip("Enables distance based fading of objects.")]
public bool FadeDetailsAtDistance = true;
[Range(0.1f, 100f)]
[Tooltip("Controls how far from the terrain Detail Distance the fading effect is visible.")]
public float FadeRange = 4;

[Min(1)]
[Tooltip("Number of instances that get updated per frame.")]
public int InstancesPerFrame = 50000;

[Tooltip("Wind effect. The dry color of Details in the terrain controls how much individual meshes are affected.")]
public WindZone WindZone;

[Tooltip("Used for all billboards.")]
public Material BillboardMaterial;

[Tooltip("Sets shadow mode of all details.")]
public ShadowCastingMode castShadows = ShadowCastingMode.On;

[Tooltip("Deactivate for higher performance in the editor. In this case, it will only work in Play Mode.")]
public bool UseInEditor = true;

[Tooltip("Compute shader to initialize the detail matrices. Necessary for the GPU positioning computations.")]
public ComputeShader IndirectInstancingComputeShader;

[Tooltip("Detail meshes may be instanced and positioned only on the GPU, or also use the CPU. GPU positioning needs hardware with compute shader capabilities.")]
public InstancingType UseProceduralInstancing = InstancingType.POS_CPU_INSTANCE_CPU;
```

## Additional information

The `InstancingType` can be one of three things:

* `POS_CPU_INSTANCE_CPU` will create the detail positions on the CPU every `UpdateInterval`and also pushes all of them to the GPU every frame (using `DrawMeshInstanced`). This is the slowest instancing type, but it works with pretty much all shaders that have GPU Instancing enabled.

* `POS_CPU_INSTANCE_GPU` will still create the detail positions on the CPU every `UpdateInterval`, but then pushes them to a `ComputeBuffer` that is used by the GPU to draw all meshes. This is significantly faster than pushing all positions with the CPU, but needs a small, additional node setup in the Shader Graph. You can read about this trick on the [blog post on ComputeShaders from CatLikeCoding](https://catlikecoding.com/unity/tutorials/basics/compute-shaders/).

  ![](shadergraph.png)
  
  As an additional benefit, every instance obtains additional information from the underlying compute shader (at the moment: a single random number). This can be used to make the instance unique. For example, butterflies flapping their wings not in sync, but independently (an example shader graph is included in the project). 
  
  <img src="butterflies.png" style="zoom:75%;" />
  
* `POS_GPU_INSTANCE_GPU` is the most sophisticated setup (just internally - it works exactly the same in the Editor). It is also the most efficient, and uses a compute shader to cull objects, which can be seen in Play Mode when observing the main camera in the Scene Editor. The same additional information (random number per instance) is available to the Shader Graphs as for `POS_CPU_INSTANCE_GPU`, so all graphs can stay the same. Note that while *not* in Play Mode,  `UseProceduralInstancing` is switched to `POS_CPU_INSTANCE_GPU` so that the GPU is not overloaded with constant data updates. In Play Mode, the `ComputeBuffer` with all detail positions is only filled once, which results in efficient instancing during runtime.
  
  <img src="cameraculling.png" style="zoom:75%;" />